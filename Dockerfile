FROM golang:1.11

ENV SONAR_SCANNER_VERSION=3.2.0.1227

RUN set -x \
  && apt-get update \
  && apt-get install -y \
    gcc \
    git \
    make \
    curl \
    wget \
    unzip \
    zip \
    jq

RUN go get -u github.com/golang/dep/cmd/dep
RUN go get -u github.com/alecthomas/gometalinter

RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-${SONAR_SCANNER_VERSION}-linux.zip && \
    unzip sonar-scanner-cli-${SONAR_SCANNER_VERSION}-linux.zip && \
    ln -s $(pwd)/sonar-scanner-3.2.0.1227-linux/bin/sonar-scanner /usr/bin/sonar-scanner

RUN mkdir /opt/resource
COPY ./assets/* /opt/resource/